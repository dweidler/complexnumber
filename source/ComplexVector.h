//
//  ComplexVector.h
//  ComplexVector
//
//  Created by Dorian Weidler on 20.01.15.
//  Copyright (c) 2015 Dorian Weidler. All rights reserved.
//

#ifndef __ComplexNew__ComplexVector__
#define __ComplexNew__ComplexVector__

#include <stdio.h>
#include "ComplexNumberNode.h"
#include <iostream>

class ComplexVector{
private:
	ComplexNumberNode* head;
	ComplexNumberNode* tail;
	
	// (De-)Constructors
	ComplexVector(const ComplexNumberNode&);
protected:
	
public:
	// (De-)Constructors
	ComplexVector();
	~ComplexVector();
	
	// Methods
	void remove();
	unsigned int size() const;
	void add(const ComplexNumberNode&);
	
};

#endif /* defined(__ComplexNew__ComplexVector__) */
