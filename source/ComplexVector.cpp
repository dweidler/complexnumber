//
//  ComplexVector.cpp
//  ComplexNew
//
//  Created by Dorian Weidler on 20.01.15.
//  Copyright (c) 2015 Dorian Weidler. All rights reserved.
//

#include "ComplexVector.h"

// Constructors
/**
 *	Author:			Dorian Weidler
 *	Method:			+<<create>>
 *	Description:	Constructor of the class.
 **/
ComplexVector::ComplexVector(){
	this->head = this->tail = NULL;
	std::cout << "<<create>> ComplexVector()";
}
/**
 *	Author:			Dorian Weidler
 *	Method:			+<<delete>>
 *	Description:	Deconstructor of the class.
 **/
ComplexVector::~ComplexVector(){
	remove();
	delete this->head;
	delete this->tail;
	std::cout << "<<delete>> ~ComplexVector()";
}

// Methods
/**
 *	Author:			Dorian Weidler
 *	Method:			+remove()
 *	Description:	Removes all elements from the list.
 **/
void ComplexVector::remove(){
	const ComplexNumberNode* next = this->head;
	const ComplexNumberNode* nextNode;
	while(next != this->tail){
		nextNode = next->getNext();
		delete(next);
		next = nextNode;
	}
	delete next;
	this->tail = this->head = NULL;
}

/**
 *	Author:			Dorian Weidler
 *	Method:			+size() const
 *  Returns:		unsigned int: Number of elements in the list.
 *	Description:	Returns the count of elements in the list.
 **/
unsigned int ComplexVector::size() const{
	if(this->head == this->tail) return 0;
	unsigned int count = 1; // Since it's checked if there's no element, it automatically starts with one in order to solve the problem with counting the last element.
	const ComplexNumberNode* next = this->head;
	while(next != this->tail){
		count += 1;
	}
	return count;
}

/**
 *	Author:			Dorian Weidler
 *	Method:			+add()
 *  Parameters:
 *      - const ComplexNumber& src: Element to add to the list.
 *	Description:	Adds an specified element to the list.
 **/
void ComplexVector::add(const ComplexNumberNode& src){
	if(head == NULL)
		this->head = new ComplexNumberNode(src);
	else{
		ComplexNumberNode* newTail = new ComplexNumberNode(src);
		this->tail->setNext(newTail);
		this->tail = newTail;
	}
}