/*
 * RealNumber.cpp
 *
 *  Created on: 12.01.2015
 *      Author: Dorian
 */

#include "RealNumber.h"
#include <iostream>

/* Initialize Serialnumber Counter */
int RealNumber::sNrCounter = 0;

/* Constructors & Deconstructors */
RealNumber::RealNumber() : real(0), sNr(getSerialNumber()) {
	std::cout << "Real() -- sNr: " << sNr << std::endl;
}

RealNumber::RealNumber(double real) : real(real), sNr(getSerialNumber()){
	std::cout << "Real(" << real << ") -- sNr: " << sNr << std::endl;
}

RealNumber::~RealNumber() {

}

/* Kopierzuweisungsoperator */
RealNumber& RealNumber::operator=(const RealNumber& src){
	this->real = src.real;
	return *this;
}

/* Output */
std::ostream& operator<<(std::ostream& os, const RealNumber& rn){
	os << "Real: " << rn.real;
	return os;
}

void RealNumber::print(){
	std::cout << "[" << real << ", SerialNumber: " << sNr << "]";
}

/* Getters */
double RealNumber::getReal() const{
	return real;
}
/* Other Methods */
int RealNumber::getSerialNumber(){
	return sNrCounter++;
}

