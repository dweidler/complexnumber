/**
 * Created by Dorian Weidler
 **/

#include <cstdlib>
#include <iostream>
#include "ComplexNumber.h"

int ComplexNumber::sNrCounter = 0;

ComplexNumber::~ComplexNumber(){
	std::cout << "~ComplexNumber()";
	print();
	std::cout << std::endl;
}

ComplexNumber::ComplexNumber(double real, double imaginary, char* name) : imaginary(imaginary), name(name){
	this->real = real;
	std::cout << "ComplexNumber()";
	setSerial();
	print();
	std::cout << std::endl;
}

ComplexNumber::ComplexNumber(double real) : imaginary(0){
	this->real = real;
	std::cout << "ComplexNumber()";
	setSerial();
	print();
	std::cout << std::endl;
}

ComplexNumber::ComplexNumber(const ComplexNumber& cNumber){
	std::cout << "ComplexNumber(const ComplexNumber&)";
	this->real = cNumber.real;
	this->imaginary = cNumber.imaginary;
	this->name = copyname(cNumber.name);
	setSerial();
	print();
	std::cout << std::endl;
}

char* ComplexNumber::copyname(char* src){
	int i = 0;
	for(i = 0; src[i] != '\0'; i+=1);
	char* name = new char[i+1];
	for(int x = 0; x <= i; x+=1){
		name[x] = src[x];
	}
	return name;
}

ComplexNumber& ComplexNumber::operator= (const ComplexNumber& src){
	ComplexNumber tmp(src);
	this->swap(tmp);
	std::cout << "ComplexNumber& operator=(const ComplexNumber&)";
	print();
	std::cout << std::endl << "Source Objekt: ";
	print(src);
	std::cout << std::endl;
	return *this;
}

void ComplexNumber::swap(ComplexNumber& other){
	ComplexNumber* tmp = new ComplexNumber(other);
	other.real = this->real;
	other.imaginary = this->imaginary;
	other.sNr = this->sNr;
	other.name = copyname(this->name);
	
	this->real = tmp->real;
	this->imaginary = tmp->imaginary;
	this->sNr = tmp->sNr;
	this->name = copyname(this->name);
	delete tmp;
}

void ComplexNumber::print(){
	std::cout << "[" << real << ", " << imaginary << ", SerialNumber: " << sNr << ", Name: " << name << "]";
}

void ComplexNumber::print(const ComplexNumber& obj){
	std::cout << "[" << (&obj)->real << ", " << (&obj)->imaginary << ", SerialNumber: " << (&obj)->sNr << ", Name: " << name << "]";
}

void ComplexNumber::addToReal(double real){
	real += real;
}

void ComplexNumber::addToImaginary(double imaginary){
	imaginary += imaginary;
}

void ComplexNumber::add(double real, double imaginary){
	this->real += real;
	this->imaginary += imaginary;
}

void ComplexNumber::setSerial(){
	sNr = sNrCounter;
	sNrCounter += 1;
}

/* Mathematical Operations */

/* Overriding operator methods */
/*ComplexNumber& ComplexNumber::operator+=(const ComplexNumber& a){
	this->imaginary += a.imaginary;
	this->real += a.real;
	print();
	std::cout << std::endl;
	return *this;
 }*/
void ComplexNumber::add(const ComplexNumber& src){
	this->real += src.real;
	this->imaginary += src.imaginary;
	print();
	std::cout << std::endl;
}
void ComplexNumber::substract(const ComplexNumber& src){
	this->real -= src.real;
	this->imaginary -= src.imaginary;
	print();
	std::cout << std::endl;
}
void ComplexNumber::divide(const ComplexNumber& a){
	this->real = (this->real * a.real + this->imaginary * a.imaginary) / (a.real * a.real + a.imaginary * a.imaginary);
	this->imaginary = (a.real * this->imaginary - this->real * a.imaginary)/(a.real * a.real + a.imaginary * a.imaginary);
	print();
	std::cout << std::endl;
}

void ComplexNumber::multiply(const ComplexNumber& a){
	this->real = (this->real * a.real - this->imaginary * a.imaginary);
	this->imaginary = (this->real * a.imaginary + a.real * this->imaginary);
	print();
	std::cout << std::endl;
}

/*ComplexNumber ComplexNumber::operator+(const ComplexNumber& a){
	ComplexNumber result(0, 0);
	result.imaginary = this->imaginary + a.imaginary;
	result.real = this->real + a.real;
	
	print(result);
	std::cout << std::endl;
	return result;
 }

ComplexNumber ComplexNumber::operator-(const ComplexNumber& a){
	ComplexNumber result(0, 0);
	result.imaginary = this->imaginary - a.imaginary;
	result.real = this->real - a.real;
	print(result);
	std::cout << std::endl;
	return result;
 }
 
 ComplexNumber ComplexNumber::operator*(const ComplexNumber& a){
	ComplexNumber result(0, 0);
	result.imaginary = (this->real * a.imaginary + a.real * this->imaginary);
	result.real = (this->real * a.real - this->imaginary * a.imaginary);
	print(result);
	std::cout << std::endl;
	return result;
 }
 
 ComplexNumber ComplexNumber::operator/(const ComplexNumber& a){
	ComplexNumber result(0, 0);
	result.imaginary = (a.real * this->imaginary - this->real * a.imaginary)/(a.real * a.real + a.imaginary * a.imaginary);
	result.real = (this->real * a.real + this->imaginary * a.imaginary) / (a.real * a.real + a.imaginary * a.imaginary);
	print(result);
	std::cout << std::endl;
	return result;
 }*/

/* Output method */
std::ostream& operator<<(std::ostream& os, const ComplexNumber& cn){
	os << "Real: " << cn.real << "; Imaginary: " <<  cn.imaginary << "; Name: " << cn.name;
	return os;
}

/* Compare Method */
bool operator==(const ComplexNumber& a, const ComplexNumber& b){
	if(a.real == b.real && a.imaginary == b.imaginary)
		return true;
	else
		return false;
}

/* Pre- and postincrementoperators */
ComplexNumber& ComplexNumber::operator++(){
	// Preinkrement
	this->real += 1;
	return *this;
}

ComplexNumber ComplexNumber::operator++(int){
	// Postinkrement
	ComplexNumber result(*this);
	++(*this);
	return result;
}
