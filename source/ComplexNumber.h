#ifndef COMPLEX_NUMBER_H

#define COMPLEX_NUMBER_H

#include "RealNumber.h"

class ComplexNumber : public RealNumber{
private:
	double imaginary = 0;
	char* name;

	char* copyname(char*);
	
	ComplexNumber(int);
	static int sNrCounter;
	int sNr;
public:
	/* Constructors */
	ComplexNumber(double, double, char*);	// Regular constructor
	ComplexNumber(double);			// Single argument constructor
	//ComplexNumber();				// Aufgabe (o)
	ComplexNumber(const ComplexNumber&);
	ComplexNumber& operator= (const ComplexNumber&);
	
	/* Output operator */
	friend std::ostream& operator<<(std::ostream&, const ComplexNumber&);
	friend bool operator==(const ComplexNumber&, const ComplexNumber&);

	/* Pre- and postincrementoperators */
	ComplexNumber& operator++();	// Postincrement
	ComplexNumber operator++(int);	// Preincrement

	/* Deconstructors */
	~ComplexNumber();
	/* Methods */
	void addToReal(double);
	void addToImaginary(double);
	void print();
	void print(const ComplexNumber& );
	void swap(ComplexNumber&);
	void add(double, double);
	void setSerial();
	
	/* Mathematical Operations */
	void add(const ComplexNumber&);
	void substract(const ComplexNumber&);
	void divide(const ComplexNumber&);
	void multiply(const ComplexNumber&);
	
	/* Overriding operator methods */
	/*ComplexNumber& operator+=(const ComplexNumber&);
		//ComplexNumber operator+(const ComplexNumber&);
		ComplexNumber operator-(const ComplexNumber&);
		ComplexNumber operator*(const ComplexNumber&);
		ComplexNumber operator/(const ComplexNumber&);*/
};

#endif
