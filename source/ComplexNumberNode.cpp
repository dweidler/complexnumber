//
//  ComplexNumberNode.cpp
//  ComplexNumberNew
//
//  Created by Dorian Weidler on 20.01.15.
//  Copyright (c) 2015 Dorian Weidler. All rights reserved.
//

#include "ComplexNumberNode.h"


// Methods
/**
 *	Author:			Dorian Weidler
 *	Method:			+getNext()
 *	Returns:		const ComplexNumber*: next list element
 *	Description:	Returns the next list element.
 **/
const ComplexNumberNode* ComplexNumberNode::getNext() const{
	return this->next;	
}
/**
 *	Author:			Dorian Weidler
 *	Method:			+setNext()
 *  Parameters:		
 *		- ComplexNumberNode* next: Represents the next node in the list.
 *	Description: Sets the next list element to this current one.
 **/
void ComplexNumberNode::setNext(const ComplexNumberNode* next){
	this->next = next;
}

/**
 *	Author:			Dorian Weidler
 *	Method:			+<<create>>
 *  Parameters:
 *      - ComplexNumberNode& src: Source element to copy.
 *	Description: Creates a new element based on the element given.
 **/
ComplexNumberNode::ComplexNumberNode(const ComplexNumberNode& src){
	this->next = new ComplexNumberNode(*src.next);
	std::cout << "<<create>> ComplexNumberNode(ComplexNumber&)";
}