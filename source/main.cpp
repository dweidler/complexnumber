// Uebung7.cpp : Definiert den Einstiegspunkt f¸r die Konsolenanwendung.
//
#include <cstdlib>
#include <unistd.h>
#include <iostream>
#include "ComplexNumber.h"
#include "RealNumber.h"
void getFromConsole(int, char*[]);

/*ComplexNumber operator+(const ComplexNumber& a, const ComplexNumber& b){
	ComplexNumber result = a;
	result.add(b);
	return result;
}*/

ComplexNumber add(const ComplexNumber& a, const ComplexNumber& b){
	ComplexNumber result(a);
	result.add(b);
	return result;
}

ComplexNumber substract(const ComplexNumber& a, const ComplexNumber& b){
	ComplexNumber result(a);
	result.substract(b);
	return result;
}

ComplexNumber multiply(const ComplexNumber& a, const ComplexNumber& b){
	ComplexNumber result(a);
	result.multiply(b);
	return result;
}

ComplexNumber divide(const ComplexNumber& a, const ComplexNumber& b){
	ComplexNumber result(a);
	result.divide(b);
	return result;
}

void swap(ComplexNumber& a, ComplexNumber& b){
	b.swap(a);
}

int main(int argc, char* argv[])
{
	//getFromConsole(argc, argv);
	ComplexNumber a(1, 2, "a");
	RealNumber* c = &a;
	a.print();
	std::cout << std::endl;
	c->print();
	std::cout << std::endl;	
	
}

void getFromConsole(int argc, char* argv[]){
	if (argc == 2)
		ComplexNumber a(atof(argv[1]));
	else if (argc == 3)
		ComplexNumber a(atof(argv[1]), atof(argv[2]), "unknown");
	else
		std::cout << "Correct use: Uebung7 [DOUBLE] [DOUBLE]." << std::endl << argv[0] << std::endl << argv[0] << " 1 3" << std::endl;
}
