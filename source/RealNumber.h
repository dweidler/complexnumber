/*
 * RealNumber.h
 *
 *  Created on: 12.01.2015
 *      Author: Dorian
 */

#ifndef REALNUMBER_H_
#define REALNUMBER_H_

#include <iostream>

class RealNumber {
protected:
	double real;

private:
	static int sNrCounter;
	int sNr;

	int getSerialNumber();

public:
	RealNumber();
	RealNumber(double);
	virtual ~RealNumber();

	/* Getters */
	double getReal() const;

	/* Kopierzuweisungsoperator */
	RealNumber& operator=(const RealNumber&);

	/* Output Method */
	friend std::ostream& operator<<(std::ostream&, const RealNumber&);
	virtual void print();
};

#endif /* REALNUMBER_H_ */
