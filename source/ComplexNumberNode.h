//
//  ComplexNumberNode.h
//  ComplexNumberNew
//
//  Created by Dorian Weidler on 20.01.15.
//  Copyright (c) 2015 Dorian Weidler. All rights reserved.
//

#ifndef __ComplexNumberNew__ComplexNumberNode__
#define __ComplexNumberNew__ComplexNumberNode__

#include <stdio.h>
#include "ComplexNumber.h"

class ComplexNumberNode{
private:
	const ComplexNumberNode* next;
protected:
	
public:
	// (De-)Constructors
	ComplexNumberNode(const ComplexNumberNode&);
	// Methods
	const ComplexNumberNode* getNext() const;
	void setNext(const ComplexNumberNode* next);
	
};

#endif /* defined(__ComplexNumberNew__ComplexNumberNode__) */
